# Best Time to Buy and Sell Stock

> You are given an array prices where prices[i] is the price of a given stock on the ith day.
You want to maximize your profit by choosing a single day to buy one stock and choosing a different day in the future to sell that stock.
Return the maximum profit you can achieve from this transaction. If you cannot achieve any profit, return 0.

 
```
Example 1:
    Input: prices = [7,1,5,3,6,4]
    Output: 5
    Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
    Note that buying on day 2 and selling on day 1 is not allowed because you must buy before you sell.

Example 2:
    Input: prices = [7,6,4,3,1]
    Output: 0
    Explanation: In this case, no transactions are done and the max profit = 0.
```
**Algorithm**
```
algorithm maximumProfit(stocks):
    buying_at = 0
    selling_at = 1
    
    while selling_at < stocks.size do
        // profit
        if selling_price > buying_price then
            max_profit = max(currentProfit, buying_price - selling_price)
        else
            buying_at = selling_at
        end if
            selling_at += 1
    end while
    return max_profit
```

**Python Code**

```py
def maximumProfit(stocks):
    buying_at = 0
    selling_at = 1
    
    max_profit = 0
    while selling_at < len(stocks):
        if stocks[selling_at] > stocks[buying_at]:
            max_profit = max(max_profit, stocks[selling_at] - stocks[buying_at])
        else:
            buying_at = selling_at
        selling_at += 1
    return max_profit
```