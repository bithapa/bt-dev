# Contains Duplicate

> Given an integer array nums, return true if any value appears at least twice in the array, and return false if every element is distinct.

 
```
Example 1:
    Input: nums = [1,2,3,1]
    Output: true

Example 2:
    Input: nums = [1,2,3,4]
    Output: false

Example 3:
    Input: nums = [1,1,1,3,3,4,3,2,4,2]
    Output: true
```
**Python Solution**
```py
def containsDuplicate(self, nums: List[int]) -> bool:
    nums.sort()
    current = nums[0]
    for item in nums[1:]:
        if item == current:
            return True
        else:
            current = item
    return False
```