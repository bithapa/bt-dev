# Print Matrix In Spiral Order
> Given a 2D matrix, print it in spiral order

**Example:**
```
Input: [[100,110,120,130,140],
        [230,240,250,260,150],
        [220,290,280,270,160],
        [210,200,190,180,170]]
        
Output: [100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 260, 270, 280, 290]
```

---
Solution

---

**Algorithm**
```
Algorithm: printMatrixSpiral(matrix):
    result += matrix.pop(0)         // add the first row to result

    if matrix and matrix[0] then    // add the last element of each remaining rows
        for row in matrix do
            result.append(row.pop())
        end for
    end if
    
    if matrix then                  // add all the elements from the last
        result += matrix.pop()[::-1]
    end if
    
    if matrix and matrix[0] then    // add the first element of each rows
        for row in matrix[::-1] do
            result.append(row.pop(0))
        end for
    end if
```

**Pyhton Implementation**

```py
def printMatrixSpiral(matrix):
    while matrix:


        # add first row
        result += matrix.pop(0)
        print("step 1: add first row: ", result)
        print(" ")


        # add last elements of each row
        if matrix and matrix[0]:
            for row in matrix:
                result.append(row.pop())
                print("step 2: add last elements: ", result)
            print(" ")



        # add last row (reverse)
        if matrix:
            result += matrix.pop()[::-1]
            print("step 3: add last row: ", result)
            print(" ")


        # add the first elements of each row (reverse)
        if matrix and matrix[0]:
            for row in matrix[::-1]:
                result.append(row.pop(0))
                print("step 4: add first elements: ", result)
            print(" ")
```