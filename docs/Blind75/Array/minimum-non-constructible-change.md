# Minimum Non-Constructible Change
> Given a list of coin values, what is the minimum change that you cannot create using the coins?

**Examples:**
```
Input: [5,7,1,1,2,3,22]
Output: 20

Input: [1,7,1,2]
Output: 5

Input: [1,4]
Output: 2

Input: [3,2,5,3]
Output: 1
```


---
Solution:

---

- sort the list
- variable x = track the change (keeps the cumulative sum of list items)
- for each item in sorted list 
    - we can make change from 0 to x if x+1 is greater or equal to next element
    - if x+1 is less than next element then x+1 is the minimum required change



**Algorithm:**


```
algorithm minimumChange(List)
    cummulativeSum = 0
    sort(List)
    for item in List do
        if cummulativeSum + 1 < item then
            return cummulativeSum + 1
        else
            cummulativeSum = cummulativeSum + item
    return cummulativeSum + 1
```

**Python Implementation:**
```py
def minimumChange(List):
    List.sort()
    cumSum = 0
    for item in List:
        if cumSum + 1 < item:
            return cumSum + 1
        else:
            cumSum = cumSum + item
    return cumSum + 1
```
