# Product of Array Except Self
> Given an integer array `nums`, return an array answer such that `answer[i]` is equal to the product of all the elements of nums except `nums[i]`.
The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit integer.
You must write an algorithm that runs in O(n) time and without using the division operation.

 
```
Example 1:
    Input: nums = [1,2,3,4]
    Output: [24,12,8,6]

Example 2:
    Input: nums = [-1,1,0,-3,3]
    Output: [0,0,9,0,0]
```

---
Soltuion

---

### Brute Force Solution

```py
 def producExceptSelfNaive(array):
    left = 0 
    right = len(array) - 1
    leftProduct = 1
    rightProduct = 1
    result = []

    for index in range(len(array)):
        while left < index:
            leftProduct = leftProduct * array[left]
            left = left+1
        while right > index:
            rightProduct = rightProduct * array[right]
            right = right-1
        
        result.append(leftProduct*rightProduct)
        
        left = 0
        right = len(array)-1
        leftProduct = 1
        rightProduct = 1
    
    return result
```

### Optimized Solution

```py
def productExceptSelfFast(nums):
    result = []
    leftRunningProduct = 1
    for i in range(len(nums)):
        result.append(leftRunningProduct)
        leftRunningProduct *= nums[i]
    rightRunningProduct = 1
    for i in reversed(range(len(nums))):
        result[i] *= rightRunningProduct
        rightRunningProduct *= nums[i]
    return result
```