# Maximum Sum Subarray


> Given an integer array nums, find the subarray with the largest sum, and return its sum.

 
```
Example 1:
    Input: nums = [-2,1,-3,4,-1,2,1,-5,4]
    Output: 6
    Explanation: The subarray [4,-1,2,1] has the largest sum 6.

Example 2:
    Input: nums = [1]
    Output: 1
    Explanation: The subarray [1] has the largest sum 1.

Example 3:
    Input: nums = [5,4,-1,7,8]
    Output: 23
    Explanation: The subarray [5,4,-1,7,8] has the largest sum 23.
```

---
Solution

---

- say the given array is `[-2,1,-3,4,-1,2,1,-5,4]`
- you iterate from index 0 to index 8
- at each iteration you keep track of the sum up to that index (say **currentSum**) and check whether you need to update it to the maximum sum (BUT HOW REALLY?)
- well, here is an idea: at given index, **if the total sum (currentSum) up to previous index was negative then you can disregard the previus negative sum (whaaaat???)**
- and you add the the current number to the sum
- and finally check and update the maximum sum
- the following table shows what happens at every iteration:

|Index i  |Number  |currentSum(at index i-1)   |Is currentSum < 0 ?|currentSum  |maxSum=max(maxSum,currentSum)  |
|---------|--------|---------------------------|-------------------|------------|-------------------------------|
|0        |-2      |0                          |no -> continue.    |-2          |-2                             |
|1        |1       |-2                         |yes -> reset to 0. |0+1=1       |1                              | 
|2        |-3      |1                          |no -> continue     |1-3=-2      |1                              | 
|3        |4       |-2                         |yes -> reset to 0  |0+4=4       |4                              | 
|4        |-1      |4                          |no -> continue     |4-1=3       |4                              | 
|5        |2       |3                          |no -> continue     |3+2=5       |5                              | 
|6        |1       |5                          |no -> continue     |5+1=6       |6                              | 
|7        |-5      |6                          |no -> continue     |6-5=1       |6                              | 
|8        |4       |1                          |no -> continue     |1+4=5       |6                              | 

---
Algorithm: Maximum SubArray

---

```
Algorithm maxSubArray(nums):
    
    currentSum = 0
    maxSum = nums[0]
    
    for n in nums do:
        
        if currentSum < 0 then:
            currentSum = 0      // reset the current track of sum only if previous currentSum was negative
        end if
        
        currentSum += n         // add the number to current sum
        maxSum = max(currentSum, maxSum)
        
    end for
    
    return maxSum
```

**Python Implementation**

```py
def maxSubArray(nums):
    
    sum = 0
    maxSum = nums[0]
    
    for n in nums:
        if sum < 0:
            sum = 0
        
        sum += n
        maxSum = max(sum, maxSum)
        
    return maxSum
```