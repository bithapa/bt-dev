# Git and Version Control

## Centralized vs Distributed version control system

## Git vs GitHub

> Git is a version control system that lets you manage and keep track of your source code history. 
> GitHub is a cloud-based hosting service that lets you manage Git repositories.

## How to push a repo to GitHub using Git

| No. | Command                                           | Description                                                                  |
|-----|---------------------------------------------------|------------------------------------------------------------------------------|
| 1   | `git init`                                        | Initializes the git repo on current working directory (creats `.git` folder) |
| 2   | `git add .`                                       | Pushes all the files into the staging area                                   |
| 3   | `git commit -m "message"`                         | commits changes to local repository with the *message*                       |   
| 4   | `git remote add <repo name> <HTTPS link to repo>` | adds the repo to remote location                                             |
| 5   | `git push --set-upstream <repo name> master`      | creates the **master** branch and uploads the code                           |

