
# Java Methods
* A block of code that runs only when it is called.
* takes parameter(s) as input. Parameters are optional.
* May or may not return certain value.
``` java
public class Main() {
  static int getTotal(int price1, int price2, boolean tip) {
    if (!tip)
      return price1 + price2;
    return price1 + price2 + 10; 
  }
}
```
* In above example, `static` means that the method `getTotal()` belongs to `Main` class.
* `int` means that the method returns a value of `int` type.
* The method takes three parameters.

## The `main` method

``` java
    public static void main(String[] args) {
    
    }
```

* public - main method should always be public so that JVM can call it from anywhere
* static -
* void - does not return anything
* String array - main method accepts one argument of type `String` array from the CLI

---