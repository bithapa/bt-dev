# Why Java?
* Java has been in the tech world for a long time (since 1995) and has a good community base
* Used in Entreprise Applications, Android application
    * Libraries →
    * Framework → Spring, Hibernate
* Write once, run everywhere
    * Platform independent → Runs on JVM on top of OS
* Java inspired languages → Kotlin, Scala, Groovy
* 11% job growth

