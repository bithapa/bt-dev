# Logical Statements and Iterations
## IF/ELSE statement 

* The method `getTotal()` below checks whether tip is given or not
* If tip is given then returns total without adding the tip
* Else adds 10$ tip to total and returns total
``` java
public class Main() 
{
  static float getTotal(float price1, float price2, boolean tip)
  {
    float beforeTipTotal = price1 + price2;
    float afterTipTotal = beforeTipTotal + 10;
    
    if (tip)
      return afterTipTotal;
    else
      return beforeTipTotal;
  }
}
```

## Switch Statement

* Instead of many `if/else` statement, a single switch statement can be used

**Syntax:**

```
switch (expression) {
    case a:
        // code block executed when expression = case a
        break;
    case b:
        // code block
        break;
    default:
        // code block       
;
```

* The `expression` is evaluated with each case inside the switch statement,
* Only the code block of case that evaluates to expression is executed,
* Keywords `break` and `default` are optional
  * Once a code block of a case is executed, `break` will prevent the program from evaluating any further cases
  * If all cases are false, then `default` case is executed
* nested `switch` statements are also possible
* `expression` can be anything eg. `integer` or `String`



---

## `for` Loop

## `while` Loop

## `do/while` Loop