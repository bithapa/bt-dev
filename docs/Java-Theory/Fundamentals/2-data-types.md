
# Variables & Data Types

* A variable in Java must be a specific data types.
* Variable names should be unique.
* 
* There are 8 primitive data types:

## Primitive data types
* A primitive data type has a specific size and type of variable.
* It has no additional methods.

| type    | size (bits) |
|---------|-------------|
| byte    | 8           | 
| short   | 16          |  
| int     | 32          |
| long    | 64          |
| float   | 32          |
| double  | 64          |
| char    | 16          |
| boolean | 1           |

## Non-Primitive data types
* String is a non-primitive data types

## Declaring constants
* A constant is a variable whose value cannot be changed throughout the program.
* A constant is declared using the keyword `final`.
* It must be initialized.
* You cannot change its value.

``` java
final int YEAR = 2023;
final String NAME = "Max";
```
* In above example a constants `YEAR` of type `int` and `NAME` of type `string`is declared.

---