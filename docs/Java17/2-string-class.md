# String Class

## String features in Java

* Immutable -- once string object is created, they cannot be changed. 
    * If modified, a new string is created in the memory.
* In C++, string are stored as an array of chars, and are null-terminated--contains null character (`\0`) at the end.
* Robust manipulation:
    * concatenation, substring, searching, replacing etc.
* Reference type -- string are stored in heap memory and manipulated via reference.
* can be created using two ways:
    * `String str = "Hello";`
    * `String str = new String("Hello");`

## String Methods

`String str = "Hello World!"`

| No. | Method                           | Description                                                  |
|-----|----------------------------------|--------------------------------------------------------------|
| 1   | `int length()`                   | returns the number of characters.                            |
| 2   | `char charAt(int i)`             | returns the char at ith index.                               |
| 3   | `String substring(int i)`        | returns the substring from ith index to end.                 |
| 4   | `String substring(int i, int j)` | returns substring from ith index to j-1                      |
| 5   | `String concat(String s)`        | concatenates s to the string and returns the result          |
| 6   | `int indexOf(String s)`          | returns the index of first occurrence of s within the string |
| 7   | ``                               ||