# Scanner
> `import java.util.Scanner`

* Java provides scanner class to **"parse primitive types and strings using regular expression"**

```java linenums="1" hl_lines="1 6 9 12 15"
import java.util.Scanner;

public class ScannerTest {
    public static void main(String[] args) {
        // scanner object is used to read the input
        Scanner scanner = new Scanner(System.in);
        
        // read integer
        int intNum = scanner.nextInt();
        
        // read double
        double dubNum = scanner.nextDouble();
        
        // read String
        String str = scanner.next(); 
    }
}
```

* **ISSUE**: When reading two inputs: first double and then string. String is not read using `nextLine()`.
* This is because `nextDouble()` won't read the end of line character instead it will be read by `nextLine()`.

```java
    // str won't be read
    double dubnum = scanner.nextDouble();
    String str = scanner.nextLine();
```

* **FIX**: Use `next()`. Reads the next token.


```java
    // str will be read
    double dubnum = scanner.nextDouble();
    String str = scanner.next();
```

*[For more, see this reference from oracle.](https://docs.oracle.com/javase/8/docs/api/java/util/Scanner.html)*